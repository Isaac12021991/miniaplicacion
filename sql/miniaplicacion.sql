-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-01-2019 a las 06:45:57
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `miniaplicacion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `i00t_cartelera_viajes`
--

CREATE TABLE `i00t_cartelera_viajes` (
  `id` int(11) NOT NULL,
  `nb_viaje` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `nu_codigo` int(11) NOT NULL,
  `nb_origen` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `nb_destino` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `nu_plazas` int(11) NOT NULL,
  `nu_precio` bigint(20) NOT NULL,
  `ff_sistema` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `in_activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla cartelera de viajes';

--
-- Volcado de datos para la tabla `i00t_cartelera_viajes`
--

INSERT INTO `i00t_cartelera_viajes` (`id`, `nb_viaje`, `nu_codigo`, `nb_origen`, `nb_destino`, `nu_plazas`, `nu_precio`, `ff_sistema`, `in_activo`) VALUES
(10, 'Milano', 20190, 'Milano', 'Venezuela', 2, 1200, '2019-01-07 05:45:01', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `i00t_personas`
--

CREATE TABLE `i00t_personas` (
  `id` int(11) NOT NULL,
  `co_usuario` int(11) NOT NULL,
  `nb_persona` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `nu_cedula` int(20) NOT NULL,
  `tx_direccion` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `nu_telefono` int(20) NOT NULL,
  `ff_sistema` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `in_activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla principal de personas viajeras';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$06$iX2aO0rLhkEuPD/NLuupR.Z2vqH5utCktzs8lhbARCsD3.9MtHY76', '', 'admin@admin.com', '', NULL, NULL, 'G9sxLutqH0iy8PN2rwQ5Wu', 1268889823, 1546839046, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `x00t_personas_viajes`
--

CREATE TABLE `x00t_personas_viajes` (
  `id` int(11) NOT NULL,
  `co_personas` int(11) NOT NULL,
  `co_cartelera_viajes` int(11) NOT NULL,
  `ff_sistema` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `in_activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de relacion de personas cartelera viajes';

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `i00t_cartelera_viajes`
--
ALTER TABLE `i00t_cartelera_viajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `i00t_personas`
--
ALTER TABLE `i00t_personas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Nro de cedula unico` (`nu_cedula`);

--
-- Indices de la tabla `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indices de la tabla `x00t_personas_viajes`
--
ALTER TABLE `x00t_personas_viajes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Clave relacion personas co_personas` (`co_personas`),
  ADD KEY `Clave relacion cartelera_viajes co_cartelera_viajes` (`co_cartelera_viajes`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `i00t_cartelera_viajes`
--
ALTER TABLE `i00t_cartelera_viajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `i00t_personas`
--
ALTER TABLE `i00t_personas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `x00t_personas_viajes`
--
ALTER TABLE `x00t_personas_viajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `x00t_personas_viajes`
--
ALTER TABLE `x00t_personas_viajes`
  ADD CONSTRAINT `Clave relacion cartelera_viajes co_cartelera_viajes` FOREIGN KEY (`co_cartelera_viajes`) REFERENCES `i00t_cartelera_viajes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Clave relacion personas co_personas` FOREIGN KEY (`co_personas`) REFERENCES `i00t_personas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
