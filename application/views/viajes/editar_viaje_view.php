<div class="page-wrapper-row full-height">
  <div class="page-wrapper-middle">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
              <h1>Editar viaje</h1>
            </div>
          </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
          <div class="container">
            <!-- BEGIN PAGE BREADCRUMBS -->
            <div class="row">
              <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ">
                <ul class="page-breadcrumb breadcrumb">
                  <li>
                  <i class="fa fa-circle"></i>
                  <a href="<?php echo site_url('inicio/index')?>">Inicio</a>
                  </li>
                  <li>
                  <i class="fa fa-circle"></i>
                  <a href="<?php echo site_url('viajes/index')?>">Viajes</a>
                  </li>
                  <li>
                  <i class="fa fa-circle"></i>
                  <a href="#">Editar viaje</a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
              </div>
            </div>
            <!-- END PAGE BREADCRUMBS -->
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="page-content-inner">
              <div class="portlet light bordered">
                <div class="portlet-title">
                  <div class="caption">
                     Editar Viaje
                  </div>
                  <div class="tools">
                  </div>
                  <div class="actions">
                  </div>
                </div>
                <div class="portlet-body">
                  <form class="form-horizontal" id="form_sample_1" role="form">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                      <div class="form-body">
                        <div class="form-group">
                          <label class="col-md-3 control-label"><b>Cedula</b></label>
                          <div class="col-md-9">
                            <input type="text" maxlength="20" name="nu_cedula" id="nu_cedula" autofocus="autofocus" class="form-control input-sm input-small" placeholder="N° Cedula" value="<?php echo $info_personas_viajes->nu_cedula; ?>">
                            <span class="help-inline">Ingrese su numero de cedula</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-3 control-label"><b>Nombre y apellido</b></label>
                          <div class="col-md-9">
                            <input type="text" maxlength="60" name="nb_persona" id="nb_persona" class="form-control input-sm input-medium" placeholder="Nombre y apellido" value="<?php echo $info_personas_viajes->nb_persona; ?>">
                            <span class="help-inline">Nombre y apellido</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-3 control-label"><b>Direccion</b></label>
                          <div class="col-md-9">
                            <textarea id="tx_direccion" maxlength="60" name="tx_direccion" class="form-control input-sm input-medium"><?php echo $info_personas_viajes->tx_direccion; ?></textarea>
                            <span class="help-inline">Direccion de domicilio de la persona</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-3 control-label"><b>Contacto</b></label>
                          <div class="col-md-9">
                            <input type="text" maxlength="8" name="nu_telefono" id="nu_telefono" class="form-control input-sm input-small" placeholder="Telefono" value="<?php echo $info_personas_viajes->nu_telefono; ?>">
                            <span class="help-inline">Ingrese un número de telefono</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <label class="col-md-3 control-label"><b>Seleccione el viaje</b></label> 
                        <div class="col-md-9">
                          <select class="form-control input-sm" id="co_cartelera_viajes" name="co_cartelera_viajes">
                            <option value="">Seleccione ...</option>
                            <?php foreach($lista_cartelera_viajes->result() as $row)
                            {

                              if($info_personas_viajes->co_cartelera_viajes == $row->id):
                                echo "<option selected='selected' value='".$row->id."'>".$row->nb_viaje."</option>";
                                continue;

                            endif;

                              echo "<option value='".$row->id."'>".$row->nb_viaje."</option>";

                             }?>
                          </select>
                          <span class="help-inline">Ingrese un numero de telefono</span>
                        </div>
                      </div>
                    </div>
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-12">
                          <a id="actualizar_viaje" class="btn btn-primary pull-right">Actualizar viaje</a>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
          </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
      </div>
      <!-- END CONTENT -->
      <!-- BEGIN QUICK SIDEBAR -->
      <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
  </div>
</div>
<script type="text/javascript">
$("#actualizar_viaje").click(function() {
        if ($('#nu_cedula').val() == '')
        {
          notificacion_toas('error','Error','Ingrese el numero de cedula');
          $('#nu_cedula').focus();
            return;
        };
        if ($('#nu_cedula').val() % 1 != 0){
          notificacion_toas('error','Error','Ingrese un número entero');
          $('#nu_cedula').focus();
            return false;
        }
        if ($('#nu_cedula').val() <= 0) {
          notificacion_toas('error','Error','Ingrese número válido');
          $('#nu_cedula').focus();
            return false;
        }
        if ($('#nb_persona').val() == '')
        {   
          $('#nb_persona').focus();
          notificacion_toas('error','Error','Ingrese el nombre de la persona viajera');
          return;
        };
      if ($('#tx_direccion').val() == '')
        {   
          $('#tx_direccion').focus();
          notificacion_toas('error','Error','Ingrese la direccion del viajero');
          return;
        };
        if ($('#nu_telefono').val() == '')
        {   
          $('#nu_telefono').focus();
          notificacion_toas('error','Error','Ingrese el numero de telefono');
          return;
        };
        if ($('#co_cartelera_viajes').val() == '')
        {   
          $('#co_cartelera_viajes').focus();
          notificacion_toas('error','Error','Seleccione un viaje destino');
          return;
        };
                                         $.ajax({
        method: "POST",
        data: {'co_personas_viajes':'<?php echo $info_personas_viajes->id; ?>','nu_cedula':$('#nu_cedula').val(), 'nb_persona':$('#nb_persona').val(), 'tx_direccion':$('#tx_direccion').val(), 'nu_telefono':$('#nu_telefono').val(), 'co_cartelera_viajes':$('#co_cartelera_viajes').val()},
        url: "<?php echo site_url('viajes/editar_viaje_ejecutar') ?>",
        beforeSend: function(){  },
                     }).done(function( data ) { 
                      var obj = JSON.parse(data);
                      if (obj.error > 0)
                      {
                        notificacion_toas('error','Error',obj.message);
                        return;
                      }else{
                    $(location).attr('href',"<?php echo site_url() ?>/viajes/index");
                      }
                      }).fail(function(){
                    notificacion_toas('error','Error','Error de conexion');
                      }); 
});
            </script>