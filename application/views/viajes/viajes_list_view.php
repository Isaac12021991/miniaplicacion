<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Inicio</h1>
                        </div>
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ">
                                <ul class="page-breadcrumb breadcrumb">
                                    <li>
                                        <i class="fa fa-circle"></i>
                                        <a href="<?php echo site_url('inicio/index')?>">Inicio</a>
                                    </li>
                                    <li>   
                                        <i class="fa fa-circle"></i>
                                        <a href="<?php echo site_url('viajes/index')?>">Viajes</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                            </div>
                        </div>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-speech"></i>
                                        <span class="caption-subject bold uppercase">Viajes</span>
                                        <span class="caption-helper">Lista de viajes registrados</span>
                                    </div>
                                    <div class="actions">
                                        <a href="<?php echo site_url('viajes/nuevo_viaje')?>"class="btn btn-circle btn-default">
                                        <i class="fa fa-plus"></i> Nuevo viaje </a>
                                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="scroller" style="height:200px" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">
                                        <?php if ($lista_viajes->num_rows() > 0) : ?>
                                        <table class="table table-advance table-hover dt-responsive" id="tabla_1" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="all" width="5%">#</th>
                                                    <th class="all" width="25%">Codigo</th>
                                                    <th class="all" width="25%">Viaje</th>
                                                    <th width="25%">Viajero</th>
                                                    <th width="25%">Precio $</th>
                                                    <th width="5%" class="all"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $con = 0; ?>
                                                <?php foreach ($lista_viajes->result() as $row) : $con ++; ?>
                                                <tr>
                                                    <td><?php echo $con; ?> </td>
                                                    <td><?php echo $row->nu_codigo; ?> </td>
                                                    <td><?php echo $row->nb_viaje; ?> </td>
                                                    <td><?php echo $row->nb_persona; ?> </td>
                                                    <td><?php echo $row->nu_precio; ?> </td>
                                                    <td>
                                                        <div class="task-config-btn btn-group">
                                                            <a class="btn btn-sm default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                            <i class="fa fa-cog"></i>
                                                            <i class="fa fa-angle-down"></i>
                                                            </a>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li>
                                                                    <a href="<?php echo site_url("viajes/editar_viaje/$row->id");?>">
                                                                    <i class="fa fa-pencil"></i> Editar</a>
                                                                </li>
                                                                <li>
                                                                    <a onclick="eliminar_viaje(<?php echo $row->id; ?>)">
                                                                    <i class="fa fa-trash"></i> Eliminar </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>   
                                            </tbody>
                                        </table>
                                        <?php else: ?>
                                        <h4>Sin registro de viajes</h4>
                                        <p></p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<script type="text/javascript">
function eliminar_viaje(co_personas_viajes) {
    var r = confirm("Estas seguro que deseas eliminar este registro");
    if (r == true) {
        $.ajax({
            method: "POST",
            data: {
                'co_personas_viajes': co_personas_viajes
            },
            url: "<?php echo site_url('viajes/eliminar_viaje') ?>",
            beforeSend: function() {},
        }).done(function(data) {
            var obj = JSON.parse(data);
            if (obj.error > 0) {
                notificacion_toas('error', 'Error', obj.message);
                return;
            } else {
                $(location).attr('href', "<?php echo site_url() ?>/viajes/index");
            }
        }).fail(function() {
            notificacion_toas('error', 'Error', 'Error de conexion');
        });
    }
}
</script>

