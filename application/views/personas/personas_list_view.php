            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container">
                                    <!-- BEGIN PAGE TITLE -->
                                    <div class="page-title">
                                        <h1>Personas / Viajeros</h1>
                                    </div>

                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">
                                <div class="container">
                                    <!-- BEGIN PAGE BREADCRUMBS -->

                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <i class="fa fa-circle"></i>
                                            <a href="<?php echo site_url('inicio/index')?>">Inicio</a>
                                        </li>

                                     <li>   
                                        <i class="fa fa-circle"></i>
                                            <a href="<?php echo site_url('viajes/index')?>">Personas</a>
                                            
                                        </li>
                                    </ul>

                                    <!-- END PAGE BREADCRUMBS -->
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">

                                  
                                       <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-speech"></i>
                                            <span class="caption-subject bold uppercase">Viajeros</span>
                                            <span class="caption-helper">Lista de viajeros en el sistema</span>
                                        </div>
                                        <div class="actions">
                                           <a href="<?php echo site_url('personas/nueva_persona')?>" class="btn btn-circle btn-default">
                                                <i class="fa fa-plus"></i>Nuevo viajero </a>
                                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                                   

              <?php if ($lista_personas->num_rows() > 0) : ?>

                  <table class="table table-advance table-hover dt-responsive" id="tabla_1" width="100%">
                    <thead>
                       <tr>
                       <th class="all" width="5%">#</th>
                        <th class="all" width="25%">N° identidad</th>
                         <th class="all" width="25%">Viajero</th>
                          <th width="25%">Direccion</th>
                          <th width="25%">Telefono</th>
                         <th width="5%" class="all"></th>
                       </tr>
                    </thead> 
                    <tbody>
                      <?php $con = 0; ?>
                      <?php foreach ($lista_personas->result() as $row) : $con ++; ?>
                          <tr>
                              <td><?php echo $con; ?> </td>
                              <td><?php echo $row->nu_cedula; ?> </td>
                              <td><?php echo $row->nb_persona; ?> </td>
                              <td><?php echo $row->tx_direccion; ?> </td>
                              <td><?php echo $row->nu_telefono; ?> </td>
                              <td>   
                        <div class="task-config-btn btn-group">
                            <a class="btn btn-sm default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="fa fa-cog"></i>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                          
                                <li>
                                    <a href="<?php echo site_url("personas/editar_persona/$row->id");?>">
                                        <i class="fa fa-pencil"></i> Editar</a>
                                </li>

                                  <li>
                                    <a onclick="eliminar_persona(<?php echo $row->id; ?>)">
                                        <i class="fa fa-trash"></i> Eliminar </a>
                                </li>


                            </ul>
                        </div>
                                        </td>
                              
                          </tr>
                       
                     <?php endforeach; ?>   
                   </tbody>

                 </table>

                                                   <?php else: ?>

                                      <h4>Sin registro de viajes</h4>
                                      <p></p>


                                    <?php endif; ?>



                                    </div>
                                </div>






                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                        <!-- BEGIN QUICK SIDEBAR -->

                        <!-- END QUICK SIDEBAR -->
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>

                        <script type="text/javascript">
              
              function eliminar_persona(co_persona) {

                      var r = confirm("Estas seguro que deseas eliminar este registro");
                            
                            if (r == true) {
                                
                  $.ajax({
        method: "POST",
        data: {'co_persona':co_persona},
        url: "<?php echo site_url('personas/eliminar_persona') ?>",
        beforeSend: function(){  },
                     }).done(function( data ) { 
                      var obj = JSON.parse(data);
                      if (obj.error > 0)
                      {
                        notificacion_toas('error','Error',obj.message);
                        return;
                      }else{
          
                      $(location).attr('href',"<?php echo site_url() ?>/personas/index");  

                      }
                      }).fail(function(){
                    notificacion_toas('error','Error','Error de conexion');
                      }); 
                               
                                }

                // body...
              }

            </script>