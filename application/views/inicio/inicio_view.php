            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container">
                                    <!-- BEGIN PAGE TITLE -->
                                    <div class="page-title">
                                        <h1>Inicio</h1>
                                    </div>

                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">
                                <div class="container">
                                    <!-- BEGIN PAGE BREADCRUMBS -->
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <i class="fa fa-circle"></i>
                                            <a href="<?php echo site_url('Inicio/index')?>">Inicio</a>
                                            
                                        </li>
                                    </ul>
                                    <!-- END PAGE BREADCRUMBS -->
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">


                                  <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-speech"></i>
                                            <span class="caption-subject bold uppercase">Mini aplicacion</span>
                                            <span class="caption-helper">Introduccion</span>
                                        </div>
                                        <div class="actions"></div>
                                    </div>
                                    <div class="portlet-body">

                                         <h4>Desarrollador: Isaac Betancourt</h4>
                                            <p>Tecnología utilizada en esta mini aplicacion de prueba: </p>
                                            <ul>                                              
                                                <li>Lenguaje principal: PHP version 7.0</li>
                                                <li>Framework de PHP: Codeigniter 3.1 MVC </li>
                                                <li>Framework javascript: Jquery version 3.3.1 </li>
                                                <li>Estilos y diseños: Bootstrap 3.0 - Responsive </li>
                                                <li>Base de datos: MySQL / motor Innodb </li>
                                            </ul>
          

                                    </div>
                                </div>
                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                        <!-- BEGIN QUICK SIDEBAR -->

                        <!-- END QUICK SIDEBAR -->
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>