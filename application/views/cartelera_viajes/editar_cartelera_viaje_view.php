<div class="page-wrapper-row full-height">
  <div class="page-wrapper-middle">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
              <h1>Editar viaje en cartelera</h1>
            </div>
          </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
          <div class="container">
            <!-- BEGIN PAGE BREADCRUMBS -->
            <div class="row">
              <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ">
                <ul class="page-breadcrumb breadcrumb">
                  <li>
                  <i class="fa fa-circle"></i>
                  <a href="<?php echo site_url('inicio/index')?>">Inicio</a>
                  </li>
                  <li>
                  <i class="fa fa-circle"></i>
                  <a href="<?php echo site_url('cartelera_viajes/index')?>">Cartelera</a>
                  </li>
                  <li>
                  <i class="fa fa-circle"></i>
                  <a href="#">Editar viaje caretelera</a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
              </div>
            </div>
            <!-- END PAGE BREADCRUMBS -->
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="page-content-inner">
              <div class="portlet light bordered">
                <div class="portlet-title">
                  <div class="caption">
                     Editar registro
                  </div>
                  <div class="tools">
                  </div>
                  <div class="actions">
                  </div>
                </div>
                <div class="portlet-body">
                  <form class="form-horizontal" id="form_sample_1" role="form">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                      <div class="form-body">
                        <div class="form-group">
                          <label class="col-md-3 control-label"><b>Nombre del viaje</b></label>
                          <div class="col-md-9">
                            <input type="text" maxlength="20" name="nb_viaje" id="nb_viaje" autofocus="autofocus" class="form-control input-sm input-small" placeholder="Viaje" value="<?php echo $info_registro_viaje_cartelera->nb_viaje; ?>">
                            <span class="help-inline">Nombre del viaje</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-3 control-label"><b>Origen</b></label>
                          <div class="col-md-9">
                            <input type="text" maxlength="60" name="nb_origen" id="nb_origen" class="form-control input-sm input-medium" placeholder="Origen" value="<?php echo $info_registro_viaje_cartelera->nb_origen; ?>">
                            <span class="help-inline">Origen del viaje</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-3 control-label"><b>Destino</b></label>
                          <div class="col-md-9">
                            <input type="text" maxlength="60" name="nb_destino" id="nb_destino" class="form-control input-sm input-medium" placeholder="Destino" value="<?php echo $info_registro_viaje_cartelera->nb_destino; ?>">
                            <span class="help-inline">Destino del viaje</span>
                          </div>
                        </div>

                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                          <label class="col-md-3 control-label"><b>Plazas</b></label>
                          <div class="col-md-9">
                            <input type="text" maxlength="8" name="nu_plazas" id="nu_plazas" class="form-control input-sm input-small" placeholder="Plazas" value="<?php echo $info_registro_viaje_cartelera->nu_plazas; ?>">
                            <span class="help-inline">Plazas</span>
                          </div>
                        </div>
                      <div class="form-group">
                          <label class="col-md-3 control-label"><b>Precio</b></label>
                          <div class="col-md-9">
                            <input type="text" maxlength="20" name="nu_precio" id="nu_precio" class="form-control input-sm input-small" placeholder="Precio en dolares" value="<?php echo $info_registro_viaje_cartelera->nu_precio; ?>">
                            <span class="help-inline">Precio</span>
                          </div>
                        </div>
                    </div>
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-12">
                          <a id="actualizar_viaje" class="btn btn-primary btn-circle btn-default pull-right">Actualizar viaje en cartelera</a>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
          </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
      </div>
      <!-- END CONTENT -->
      <!-- BEGIN QUICK SIDEBAR -->
      <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
  </div>
</div>
<script type="text/javascript">
$("#actualizar_viaje").click(function() {

        if ($('#nb_viaje').val() == '')
        {   
          $('#nb_viaje').focus();
          notificacion_toas('error','Error','Ingrese el nombre del viaje');
          return;
        };
      if ($('#nb_origen').val() == '')
        {   
          $('#nb_origen').focus();
          notificacion_toas('error','Error','Ingrese el origen del viaje');
          return;
        };
        if ($('#nb_destino').val() == '')
        {   
          $('#nb_destino').focus();
          notificacion_toas('error','Error','Ingrese el destino del viaje');
          return;
        };
        if ($('#nu_plazas').val() == '')
        {   
          $('#nu_plazas').focus();
          notificacion_toas('error','Error','Ingrese el numero de plazas');
          return;
        };
        if ($('#nu_precio').val() == '')
        {
          notificacion_toas('error','Error','Ingrese el precio del viaje');
          $('#nu_precio').focus();
            return;
        };
        if ($('#nu_precio').val() <= 0) {
          notificacion_toas('error','Error','Ingrese precio válido');
          $('#nu_precio').focus();
            return false;
        }
                                         $.ajax({
        method: "POST",
        data: {'co_cartelera_viaje':'<?php echo $info_registro_viaje_cartelera->id; ?>', 'nb_viaje':$('#nb_viaje').val(), 'nb_origen':$('#nb_origen').val(), 'nb_destino':$('#nb_destino').val(), 'nu_plazas':$('#nu_plazas').val(), 'nu_precio':$('#nu_precio').val()},
        url: "<?php echo site_url('cartelera_viajes/editar_viaje_cartelera_ejecutar') ?>",
        beforeSend: function(){  },
                     }).done(function( data ) { 
                      var obj = JSON.parse(data);
                      if (obj.error > 0)
                      {
                        notificacion_toas('error','Error',obj.message);
                        return;
                      }else{
          
                      $(location).attr('href',"<?php echo site_url() ?>/cartelera_viajes/index");  

                      }
                      }).fail(function(){
                    notificacion_toas('error','Error','Error de conexion');
                      }); 
});
            </script>