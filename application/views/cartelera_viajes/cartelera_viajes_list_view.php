<div class="page-wrapper-row full-height">
   <div class="page-wrapper-middle">
      <!-- BEGIN CONTAINER -->
      <div class="page-container">
         <!-- BEGIN CONTENT -->
         <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
               <div class="container">
                  <!-- BEGIN PAGE TITLE -->
                  <div class="page-title">
                     <h1>Cartelera de viajes</h1>
                  </div>
               </div>
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
               <div class="container">
                  <!-- BEGIN PAGE BREADCRUMBS -->
                  <ul class="page-breadcrumb breadcrumb">
                     <li>
                        <i class="fa fa-circle"></i>
                        <a href="<?php echo site_url('inicio/index')?>">Inicio</a>
                     </li>
                     <li>   
                        <i class="fa fa-circle"></i>
                        <a href="<?php echo site_url('cartelera_viajes/index')?>">Cartelera</a>
                     </li>
                  </ul>
                  <!-- END PAGE BREADCRUMBS -->
                  <!-- BEGIN PAGE CONTENT INNER -->
                  <div class="page-content-inner">
                     <div class="portlet light">
                        <div class="portlet-title">
                           <div class="caption">
                              <i class="icon-speech"></i>
                              <span class="caption-subject bold uppercase">Cartelera de viajes</span>
                              <span class="caption-helper">Lista de viajes registrados en la cartelera</span>
                           </div>
                           <div class="actions">
                              <a href="<?php echo site_url('cartelera_viajes/nuevo_cartelera_viaje')?>" class="btn btn-circle btn-default">
                              <i class="fa fa-plus"></i>Agregar nuevo viaje </a>
                              <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
                           </div>
                        </div>
                        <div class="portlet-body">
                           <?php if ($lista_cartelera_viajes->num_rows() > 0) : ?>
                           <table class="table table-advance table-hover dt-responsive" id="tabla_1" width="100%">
                              <thead>
                                 <tr>
                                    <th class="all" width="5%">#</th>
                                    <th width="15%">Viaje</th>
                                    <th class="all" width="10%">Codigo</th>
                                    <th width="10%">Precio</th>
                                    <th width="10%">Lugar origen</th>
                                    <th width="10%">Lugar destino</th>
                                    <th width="5%" class="all"></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php $con = 0; ?>
                                 <?php foreach ($lista_cartelera_viajes->result() as $row) : $con ++; ?>
                                 <tr>
                                    <td><?php echo $con; ?> </td>
                                    <td><?php echo $row->nb_viaje; ?> </td>
                                    <td><?php echo $row->nu_codigo; ?> </td>
                                    <td><?php echo $row->nu_precio; ?> </td>
                                    <td><?php echo $row->nb_origen; ?> </td>
                                    <td><?php echo $row->nb_destino; ?> </td>
                                    <td>
                                       <div class="task-config-btn btn-group">
                                          <a class="btn btn-sm default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                          <i class="fa fa-cog"></i>
                                          <i class="fa fa-angle-down"></i>
                                          </a>
                                          <ul class="dropdown-menu pull-right">
                                             <li>
                                                <a href="<?php echo site_url("cartelera_viajes/editar_cartelera_viaje/$row->id");?>">
                                                <i class="fa fa-pencil"></i> Editar</a>
                                             </li>
                                             <li>
                                                <a onclick="eliminar_cartelera_viaje(<?php echo $row->id; ?>)">
                                                <i class="fa fa-trash"></i> Eliminar </a>
                                             </li>
                                          </ul>
                                       </div>
                                    </td>
                                 </tr>
                                 <?php endforeach; ?>   
                              </tbody>
                           </table>
                           <?php else: ?>
                           <h4>Sin registro de viajes</h4>
                           <p></p>
                           <?php endif; ?>
                        </div>
                     </div>
                  </div>
                  <!-- END PAGE CONTENT INNER -->
               </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
         </div>
         <!-- END CONTENT -->
         <!-- BEGIN QUICK SIDEBAR -->
         <!-- END QUICK SIDEBAR -->
      </div>
      <!-- END CONTAINER -->
   </div>
</div>
<script type="text/javascript">
   function eliminar_cartelera_viaje(co_cartelera_viaje) {
   
           var r = confirm("Estas seguro que deseas eliminar este registro");
                 
                 if (r == true) {
                     
       $.ajax({
   method: "POST",
   data: {'co_cartelera_viaje':co_cartelera_viaje},
   url: "<?php echo site_url('cartelera_viajes/eliminar_cartelera_viaje') ?>",
   beforeSend: function(){  },
          }).done(function( data ) { 
           var obj = JSON.parse(data);
           if (obj.error > 0)
           {
             notificacion_toas('error','Error',obj.message);
             return;
           }else{
   
           $(location).attr('href',"<?php echo site_url() ?>/cartelera_viajes/index");  
   
           }
           }).fail(function(){
         notificacion_toas('error','Error','Error de conexion');
           }); 
                    
                     }
   
     // body...
   }
   
</script>

