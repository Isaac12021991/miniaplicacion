<?php
class Viajes_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    function get_viajes_model() {
        $sql   = "SELECT a.id, a.co_personas, a.co_cartelera_viajes, b.nb_persona, b.nu_cedula, b.nu_telefono, b.tx_direccion, c.nb_viaje, c.nu_precio, c.nu_codigo
    FROM x00t_personas_viajes as a
    join i00t_personas as b on b.id = a.co_personas
    join i00t_cartelera_viajes as c on c.id = a.co_cartelera_viajes
    where a.in_activo = '1'";
        $query = $this->db->query($sql);
        return $query;
    }
    function get_lista_cartelera_viajes_model() {
        $sql   = "SELECT * from i00t_cartelera_viajes as a where a.in_activo = '1'";
        $query = $this->db->query($sql);
        return $query;
    }
    function get_info_persona_model($nu_cedula) {
        $sql   = "SELECT * from i00t_personas as a where a.in_activo = '1' and a.nu_cedula = '$nu_cedula' limit 1";
        $query = $this->db->query($sql);
        return $query;
    }
    function nuevo_viaje_ejecutar_model($nu_cedula, $nb_persona, $tx_direccion, $nu_telefono, $co_cartelera_viajes) {
        $this->db->trans_start();
        $info_persona = $this->get_info_persona_model($nu_cedula);
        if ($info_persona->num_rows() > 0):
            $info_obtener_persona = $info_persona->row();
            $co_persona           = $info_obtener_persona->id;
        else:
            $i00t_personas['co_usuario']   = $this->ion_auth->user_id();
            $i00t_personas['nb_persona']   = $nb_persona;
            $i00t_personas['nu_cedula']    = $nu_cedula;
            $i00t_personas['tx_direccion'] = $tx_direccion;
            $i00t_personas['nu_telefono']  = $nu_telefono;
            $this->db->insert("i00t_personas", $i00t_personas);
            $co_persona = $this->db->insert_id();
        endif;
        $x00t_personas_viajes['co_personas']         = $co_persona;
        $x00t_personas_viajes['co_cartelera_viajes'] = $co_cartelera_viajes;
        $this->db->insert("x00t_personas_viajes", $x00t_personas_viajes);
        $this->db->trans_complete();
        return 'Registro guardado';
    }
    // Obtener un registro 
    function get_row_info_personas_viajes_model($co_personas_viajes) {
        $sql   = "SELECT a.id, a.co_personas, a.co_cartelera_viajes, b.nb_persona, b.nu_cedula, b.nu_telefono, b.tx_direccion
    FROM x00t_personas_viajes as a
    join i00t_personas as b on b.id = a.co_personas
    join i00t_cartelera_viajes as c on c.id = a.co_cartelera_viajes
    where a.id = $co_personas_viajes";
        $query = $this->db->query($sql);
        return $query->row();
    }
    // Editar
    function editar_viaje_ejecutar_model($co_personas_viajes, $nu_cedula, $nb_persona, $tx_direccion, $nu_telefono, $co_cartelera_viajes) {
        $this->db->trans_start();
        $info_persona = $this->get_info_persona_model($nu_cedula);
        if ($info_persona->num_rows() > 0):
            $info_obtener_persona          = $info_persona->row();
            $co_persona                    = $info_obtener_persona->id;
            $i00t_personas['nb_persona']   = $nb_persona;
            $i00t_personas['nu_cedula']    = $nu_cedula;
            $i00t_personas['tx_direccion'] = $tx_direccion;
            $i00t_personas['nu_telefono']  = $nu_telefono;
            $this->db->where("id", $co_persona);
            $this->db->update("i00t_personas", $i00t_personas);
        else:
            $i00t_personas['co_usuario']   = $this->ion_auth->user_id();
            $i00t_personas['nb_persona']   = $nb_persona;
            $i00t_personas['nu_cedula']    = $nu_cedula;
            $i00t_personas['tx_direccion'] = $tx_direccion;
            $i00t_personas['nu_telefono']  = $nu_telefono;
            $this->db->insert("i00t_personas", $i00t_personas);
            $co_persona = $this->db->insert_id();
        endif;
        $x00t_personas_viajes['co_personas']         = $co_persona;
        $x00t_personas_viajes['co_cartelera_viajes'] = $co_cartelera_viajes;
        $this->db->where("id", $co_personas_viajes);
        $this->db->update("x00t_personas_viajes", $x00t_personas_viajes);
        $this->db->trans_complete();
        return 'Registro actualizado';
    }
    function eliminar_viaje_model($co_personas_viajes) {
        $this->db->trans_start();
        /* Editar registro viaje */
        $x00t_personas_viajes['in_activo'] = '0';
        $this->db->where("id", $co_personas_viajes);
        $this->db->update("x00t_personas_viajes", $x00t_personas_viajes);
        $this->db->trans_complete();
        return 'Registro eliminado';
    }
}
?>