<?php
class Personas_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    function get_personas_model() {
        $sql   = "SELECT * from i00t_personas as a where a.in_activo = '1'";
        $query = $this->db->query($sql);
        return $query;
    }
    function get_info_persona_model($nu_cedula, $co_persona = 0) {
        if ($co_persona == 0):
            $query_search = "and a.nu_cedula = '$nu_cedula'";
        else:
            $query_search = "and not a.id = '$co_persona' and a.nu_cedula = '$nu_cedula'";
        endif;
        $sql   = "SELECT * from i00t_personas as a where a.in_activo = '1' $query_search limit 1";
        $query = $this->db->query($sql);
        return $query;
    }
    function nueva_persona_ejecutar_model($nu_cedula, $nb_persona, $tx_direccion, $nu_telefono) {
        $this->db->trans_start();
        $i00t_personas['co_usuario']   = $this->ion_auth->user_id();
        $i00t_personas['nb_persona']   = $nb_persona;
        $i00t_personas['nu_cedula']    = $nu_cedula;
        $i00t_personas['tx_direccion'] = $tx_direccion;
        $i00t_personas['nu_telefono']  = $nu_telefono;
        $this->db->insert("i00t_personas", $i00t_personas);
        $this->db->trans_complete();
        return $nb_persona;
    }
    // Obtener un registro 
    function get_row_info_personas_model($co_persona) {
        $sql   = "SELECT a.*
    FROM i00t_personas as a
    where a.id = $co_persona";
        $query = $this->db->query($sql);
        return $query->row();
    }
    // Editar
    function editar_persona_ejecutar_model($co_persona, $nu_cedula, $nb_persona, $tx_direccion, $nu_telefono) {
        $this->db->trans_start();
        $info_persona                  = $this->get_info_persona_model($nu_cedula);
        $i00t_personas['nb_persona']   = $nb_persona;
        $i00t_personas['nu_cedula']    = $nu_cedula;
        $i00t_personas['tx_direccion'] = $tx_direccion;
        $i00t_personas['nu_telefono']  = $nu_telefono;
        $this->db->where("id", $co_persona);
        $this->db->update("i00t_personas", $i00t_personas);
        $this->db->trans_complete();
        return 'Registro actualizado';
    }
    function eliminar_persona_model($co_persona) {
        $this->db->trans_start();
        /* Editar registro viaje */
        $i00t_personas['in_activo'] = '0';
        $this->db->where("id", $co_persona);
        $this->db->update("i00t_personas", $i00t_personas);
        $this->db->trans_complete();
        return 'Registro eliminado';
    }
}
?>