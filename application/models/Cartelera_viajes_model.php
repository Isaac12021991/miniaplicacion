<?php
class Cartelera_viajes_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    function get_registros_cartelera_viajes_model() {
        $sql   = "SELECT * from i00t_cartelera_viajes as a";
        $query = $this->db->query($sql);
        return $query;
    }
    function get_cartelera_viajes_model() {
        $sql   = "SELECT * from i00t_cartelera_viajes as a where a.in_activo = 1";
        $query = $this->db->query($sql);
        return $query;
    }
    function nuevo_viaje_cartelera_ejecutar_model($nb_viaje, $nb_origen, $nb_destino, $nu_plazas, $nu_precio) {
        $this->db->trans_start();
        /* Registro de nuevo viaje en cartelera */
        $nu_registros                        = $this->get_registros_cartelera_viajes_model();
        $consecutivo                         = $nu_registros->num_rows();
        $year                                = date('Y');
        $nu_codigo                           = $year . '' . $consecutivo++;
        $i00t_cartelera_viajes['nu_codigo']  = $nu_codigo;
        $i00t_cartelera_viajes['nb_viaje']   = $nb_viaje;
        $i00t_cartelera_viajes['nb_origen']  = $nb_origen;
        $i00t_cartelera_viajes['nb_destino'] = $nb_destino;
        $i00t_cartelera_viajes['nu_plazas']  = $nu_plazas;
        $i00t_cartelera_viajes['nu_precio']  = $nu_precio;
        $this->db->insert("i00t_cartelera_viajes", $i00t_cartelera_viajes);
        $this->db->trans_complete();
        return 'Registro guardado';
    }
    function eliminar_cartelera_viaje_model($co_cartelera_viaje) {
        $this->db->trans_start();
        $i00t_cartelera_viajes['in_activo'] = '0';
        $this->db->where("id", $co_cartelera_viaje);
        $this->db->update("i00t_cartelera_viajes", $i00t_cartelera_viajes);
        $this->db->trans_complete();
        return 'Viaje de cartelera eliminado';
    }
    function get_row_cartelera_viajes_model($co_cartelera_viaje) {
        $sql   = "SELECT * from i00t_cartelera_viajes as a where a.id = $co_cartelera_viaje";
        $query = $this->db->query($sql);
        return $query->row();
    }
    function editar_viaje_cartelera_ejecutar_model($co_cartelera_viaje, $nb_viaje, $nb_origen, $nb_destino, $nu_plazas, $nu_precio) {
        $this->db->trans_start();
        /* Editar registro de nuevo viaje en cartelera */
        $i00t_cartelera_viajes['nb_viaje']   = $nb_viaje;
        $i00t_cartelera_viajes['nb_origen']  = $nb_origen;
        $i00t_cartelera_viajes['nb_destino'] = $nb_destino;
        $i00t_cartelera_viajes['nu_plazas']  = $nu_plazas;
        $i00t_cartelera_viajes['nu_precio']  = $nu_precio;
        $this->db->where("id", $co_cartelera_viaje);
        $this->db->update("i00t_cartelera_viajes", $i00t_cartelera_viajes);
        $this->db->trans_complete();
        return 'Registro editado';
    }
}
?>