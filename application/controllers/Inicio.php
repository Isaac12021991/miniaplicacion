<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	function __construct(){

		parent::__construct();

		if (!$this->ion_auth->logged_in()) {
			redirect('auth/index');
			return;
		}

	}


	public function index()
	{	

		$this->load->view('layout/header_view');
		$this->load->view('inicio/inicio_view');
		$this->load->view('layout/footer_view');

	}
}
