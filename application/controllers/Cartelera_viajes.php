<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cartelera_viajes extends CI_Controller {
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/index');
            return;
        }
        $this->load->model('Cartelera_viajes_model', 'cartelera_viajes');
    }
    /* Inicio del modulo de cartelera de viajes */
    public function index() {
        $data['lista_cartelera_viajes'] = $this->cartelera_viajes->get_cartelera_viajes_model();
        $this->load->view('layout/header_view');
        $this->load->view('cartelera_viajes/cartelera_viajes_list_view', $data);
        $this->load->view('layout/footer_view');
    }
    /* Registro de nuevo viaje en cartelera desde el controlador en formulario */
    public function nuevo_cartelera_viaje() {
        $this->load->view('layout/header_view');
        $this->load->view('cartelera_viajes/nuevo_cartelera_viaje_view');
        $this->load->view('layout/footer_view');
    }
    /* Ejecutar el registro de nuevo viaje en cartelera, los datos recibidos del formulario de carga */
    public function nuevo_viaje_cartelera_ejecutar() {
        $error      = 0;
        $message    = '';
        $nb_viaje   = trim($this->input->post('nb_viaje'));
        $nb_origen  = trim($this->input->post('nb_origen'));
        $nb_destino = trim($this->input->post('nb_destino'));
        $nu_plazas  = trim($this->input->post('nu_plazas'));
        $nu_precio  = trim($this->input->post('nu_precio'));
        // Validacion por parte del servidor
        if ($nb_viaje == ''):
            $message .= "Ingrese el nombre del viaje";
            $error++;
            $this->retornar_respuesta($error, $message);
        endif;
        if ($nb_origen == ''):
            $message .= "Ingrese el origen del viaje";
            $error++;
            $this->retornar_respuesta($error, $message);
        endif;
        if ($nb_destino == ''):
            $message .= "Ingrese el destino del viaje";
            $error++;
            $this->retornar_respuesta($error, $message);
        endif;
        if ($nu_plazas == ''):
            $message .= "Ingrese el numero de plazas";
            $error++;
            $this->retornar_respuesta($error, $message);
        endif;
        if ($nu_precio == ''):
            $message .= "Ingrese el precio del viaje";
            $error++;
            $this->retornar_respuesta($error, $message);
        endif;
        if ($error == 0) {
            $info = $this->cartelera_viajes->nuevo_viaje_cartelera_ejecutar_model($nb_viaje, $nb_origen, $nb_destino, $nu_plazas, $nu_precio);
            $message .= $info;
        }
        $this->retornar_respuesta($error, $message);
    }
    /* Carga del formulario de editar registro de viaje en cartelera*/
    public function editar_cartelera_viaje($co_cartelera_viaje) {
        $data['info_registro_viaje_cartelera'] = $this->cartelera_viajes->get_row_cartelera_viajes_model($co_cartelera_viaje);
        $this->load->view('layout/header_view');
        $this->load->view('cartelera_viajes/editar_cartelera_viaje_view', $data);
        $this->load->view('layout/footer_view');
    }
    /* Ejecucion de la carga del formulario para editar */
    public function editar_viaje_cartelera_ejecutar() {
        $error              = 0;
        $message            = '';
        $co_cartelera_viaje = trim($this->input->post('co_cartelera_viaje'));
        $nb_viaje           = trim($this->input->post('nb_viaje'));
        $nb_origen          = trim($this->input->post('nb_origen'));
        $nb_destino         = trim($this->input->post('nb_destino'));
        $nu_plazas          = trim($this->input->post('nu_plazas'));
        $nu_precio          = trim($this->input->post('nu_precio'));
        // Validacion 1
        if ($error == 0) {
            $info = $this->cartelera_viajes->editar_viaje_cartelera_ejecutar_model($co_cartelera_viaje, $nb_viaje, $nb_origen, $nb_destino, $nu_plazas, $nu_precio);
            $message .= $info;
        }
        $this->retornar_respuesta($error, $message);
    }
    /* Eliminar un registro de la cartelera */
    function eliminar_cartelera_viaje() {
        $error              = 0;
        $message            = '';
        $co_cartelera_viaje = trim($this->input->post('co_cartelera_viaje'));
        if ($error == 0) {
            $info = $this->cartelera_viajes->eliminar_cartelera_viaje_model($co_cartelera_viaje);
            $message .= $info;
        }
        $this->retornar_respuesta($error, $message);
    }
    function retornar_respuesta($error, $message) {
        $arreglo = array(
            'error' => $error,
            'message' => $message
        );
        echo json_encode($arreglo);
        die();
    }
}
