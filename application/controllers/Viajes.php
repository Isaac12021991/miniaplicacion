<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Viajes extends CI_Controller {
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/index');
            return;
        }
        $this->load->model('Viajes_model', 'viajes');
    }
    public function index() {
        $data['lista_viajes'] = $this->viajes->get_viajes_model();
        $this->load->view('layout/header_view');
        $this->load->view('viajes/viajes_list_view', $data);
        $this->load->view('layout/footer_view');
    }
    // Nuevo viajes
    public function nuevo_viaje() {
        $data['lista_cartelera_viajes'] = $this->viajes->get_lista_cartelera_viajes_model();
        $this->load->view('layout/header_view');
        $this->load->view('viajes/nuevo_viaje_view', $data);
        $this->load->view('layout/footer_view');
    }
    //
    public function nuevo_viaje_ejecutar() {
        $error               = 0;
        $message             = '';
        $nu_cedula           = trim($this->input->post('nu_cedula'));
        $nb_persona          = trim($this->input->post('nb_persona'));
        $tx_direccion        = trim($this->input->post('tx_direccion'));
        $nu_telefono         = trim($this->input->post('nu_telefono'));
        $co_cartelera_viajes = trim($this->input->post('co_cartelera_viajes'));
        // Validacion 1
        if ($error == 0) {
            $info = $this->viajes->nuevo_viaje_ejecutar_model($nu_cedula, $nb_persona, $tx_direccion, $nu_telefono, $co_cartelera_viajes);
            $message .= $info;
        }
        $this->retornar_respuesta($error, $message);
    }
    // Editar viaje
    // Nuevo viajes
    public function editar_viaje($co_personas_viajes) {
        $data['lista_cartelera_viajes'] = $this->viajes->get_lista_cartelera_viajes_model();
        $data['info_personas_viajes']   = $this->viajes->get_row_info_personas_viajes_model($co_personas_viajes);
        $this->load->view('layout/header_view');
        $this->load->view('viajes/editar_viaje_view', $data);
        $this->load->view('layout/footer_view');
    }
    public function editar_viaje_ejecutar() {
        $error               = 0;
        $message             = '';
        $nu_cedula           = trim($this->input->post('nu_cedula'));
        $nb_persona          = trim($this->input->post('nb_persona'));
        $tx_direccion        = trim($this->input->post('tx_direccion'));
        $nu_telefono         = trim($this->input->post('nu_telefono'));
        $co_cartelera_viajes = trim($this->input->post('co_cartelera_viajes'));
        $co_personas_viajes  = trim($this->input->post('co_personas_viajes'));
        // Validacion 1
        if ($error == 0) {
            $info = $this->viajes->editar_viaje_ejecutar_model($co_personas_viajes, $nu_cedula, $nb_persona, $tx_direccion, $nu_telefono, $co_cartelera_viajes);
            $message .= $info;
        }
        $this->retornar_respuesta($error, $message);
    }
    /* Eliminar un registro de la cartelera */
    function eliminar_viaje() {
        $error              = 0;
        $message            = '';
        $co_personas_viajes = trim($this->input->post('co_personas_viajes'));
        if ($error == 0) {
            $info = $this->viajes->eliminar_viaje_model($co_personas_viajes);
            $message .= $info;
        }
        $this->retornar_respuesta($error, $message);
    }
    function retornar_respuesta($error, $message) {
        $arreglo = array(
            'error' => $error,
            'message' => $message
        );
        echo json_encode($arreglo);
        die();
    }
}
