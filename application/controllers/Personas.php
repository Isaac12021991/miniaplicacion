<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Personas extends CI_Controller {
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/index');
            return;
        }
        $this->load->model('Personas_model', 'personas');
    }
    public function index() {
        $data['lista_personas'] = $this->personas->get_personas_model();
        $this->load->view('layout/header_view');
        $this->load->view('personas/personas_list_view', $data);
        $this->load->view('layout/footer_view');
    }
    // Nuevo viajes
    public function nueva_persona() {
        $this->load->view('layout/header_view');
        $this->load->view('personas/nueva_persona_view');
        $this->load->view('layout/footer_view');
    }
    // Nuevo viajes ejecutar lo recibido del formulario
    public function nueva_persona_ejecutar() {
        $error        = 0;
        $message      = '';
        $nu_cedula    = trim($this->input->post('nu_cedula'));
        $nb_persona   = trim($this->input->post('nb_persona'));
        $tx_direccion = trim($this->input->post('tx_direccion'));
        $nu_telefono  = trim($this->input->post('nu_telefono'));
        // Validacion que el usuario este registrado en el sistema
        $info_persona = $this->personas->get_info_persona_model($nu_cedula);
        if ($info_persona->num_rows() > 0):
            $message .= "El número de cedula ya esta registrado en el sistema";
            $error++;
            $this->retornar_respuesta($error, $message);
        endif;
        if ($error == 0) {
            $info = $this->personas->nueva_persona_ejecutar_model($nu_cedula, $nb_persona, $tx_direccion, $nu_telefono);
            $message .= 'El viajero ' . $info . ' se ha registrado con éxito';
        }
        $this->retornar_respuesta($error, $message);
    }
    // Editar viaje
    // Nuevo viajes
    public function editar_persona($co_persona) {
        $data['info_persona'] = $this->personas->get_row_info_personas_model($co_persona);
        $this->load->view('layout/header_view');
        $this->load->view('personas/editar_persona_view', $data);
        $this->load->view('layout/footer_view');
    }
    public function editar_persona_ejecutar() {
        $error        = 0;
        $message      = '';
        $nu_cedula    = trim($this->input->post('nu_cedula'));
        $nb_persona   = trim($this->input->post('nb_persona'));
        $tx_direccion = trim($this->input->post('tx_direccion'));
        $nu_telefono  = trim($this->input->post('nu_telefono'));
        $co_persona   = trim($this->input->post('co_persona'));
        // Validacion que el usuario este registrado en el sistema
        $info_persona = $this->personas->get_info_persona_model($nu_cedula, $co_persona);
        if ($info_persona->num_rows() > 0):
            $message .= "El número de cedula ya esta registrado en el sistema";
            $error++;
            $this->retornar_respuesta($error, $message);
        endif;
        if ($error == 0) {
            $info = $this->personas->editar_persona_ejecutar_model($co_persona, $nu_cedula, $nb_persona, $tx_direccion, $nu_telefono);
            $message .= $info;
        }
        $this->retornar_respuesta($error, $message);
    }
    /* Eliminar un registro de la cartelera */
    function eliminar_persona() {
        $error      = 0;
        $message    = '';
        $co_persona = trim($this->input->post('co_persona'));
        if ($error == 0) {
            $info = $this->personas->eliminar_persona_model($co_persona);
            $message .= $info;
        }
        $this->retornar_respuesta($error, $message);
    }
    function retornar_respuesta($error, $message) {
        $arreglo = array(
            'error' => $error,
            'message' => $message
        );
        echo json_encode($arreglo);
        die();
    }
}
